package com.example.dell.navigation_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.LoginFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class FirstPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_page);

        Button button02 = (Button) findViewById(R.id.button_go_back_to_main_page);
        button02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("Go back to main page", "-------View.OnClickListener reached ");

                //Intent intent = new Intent(getApplicationContext(),Main2Activity.class);
                //startActivity(intent);

                Intent intent1 = new Intent(getApplicationContext(),MainPAge.class);
                startActivity(intent1);
            }
        });

            }


}
