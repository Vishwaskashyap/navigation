package com.example.dell.navigation_2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainPAge extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);


        Button button01 = (Button) findViewById(R.id.button_go_to_first_page);
        button01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("Go to First page","-----View.OnClickListener reached ");

                //Intent intent = new Intent(getApplicationContext(),Main2Activity.class);
                //startActivity(intent);
                Intent intent = new Intent(getApplicationContext(),FirstPage.class);
                startActivity(intent);

            }
        });
    }





}
